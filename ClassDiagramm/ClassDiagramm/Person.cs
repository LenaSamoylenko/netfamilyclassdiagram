﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandProj.Models
{
    public class FullName
    {
        public string Surname { get; set; }
        public string Name { get; set; }//*
        public string Patronymic { get; set; }


    }

    public class BaseItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
    }
    public class DonationItem : BaseItem
    {
        public System.Collections.Generic.List<DonationItemType> DonationItemType { get; set; }
    }
    public class DonationItemType : BaseItemType
    {
    }

    public class Adress
    {
        public string Country { get; set; }//*
        public string Region { get; set; }//*
        public string City { get; set; }//*
        public string Street { get; set; }//*
        public string House { get; set; }//#*
    }

    public class Contacts
    {
        public string Email { get; set; }//*
        public string Phone { get; set; }


    }

    public class Person
    {
        public int ID { get; set; }
        public FullName FullName { get; set; }
        public DateTime Birthday { get; set; }
        public Adress Address { get; set; }
        public Contacts Contacts { get; set; }
        public float Rating { get; set; }
        public string Avatar { get; set; }//??
    }
    public class CharityMarket : Person
    {
        public System.Collections.Generic.List<Donation> Donations { get; set; }
    }
    public class Orphan : Person
    {
        public int OrphanageId { get; set; }
        public System.Collections.Generic.List<AuctionLot> AuctionLots { get; set; }
    }
    public class Volunteer : Person
    {

    }
    public class Representative : Person
    {
        public int ID { get; set; }
        public int OrphanageId { get; set; }
    }
    public class Orphanage
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Adress Adress { get; set; }
        public float Rating { get; set; }
        public string Avatar { get; set; }

        public System.Collections.Generic.List<Representative> Representatives { get; set; }
        public System.Collections.Generic.List<Orphan> OrphansIds { get; set; }
    }

    public class Donation
    {
        public int ID { get; set; }
        public DonationItem DonationItem { get; set; }
        public DateTime Date { get; set; }
    }
    public class DonationType : BaseItem
    {

    }

    public class AuctionLot
    {
        public int ID { get; set; }
        public AuctionLotItem AuctionLotItem { get; set; }
        public DateTime Date { get; set; }
    }
    public class AuctionLotItem : BaseItem
    {
        public List<AuctionLotItemType> AuctionLotItemType
        {
            get; set;

        }
    }
    public class AuctionLotItemType : BaseItemType
    {

    }
    public class BaseItemType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<int> ChildsIds { get; set; }
        public List<int> ParentID { get; set; }
    }
}
